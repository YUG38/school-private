const config = {
  challenge: true, // Set to true if you want to enable password protection.
  users: {
    // You can add multiple users by doing username: 'password'.
    Yug: "YUGONTOP",
    Hibbrya79: "704279",
    Komasanzzz: "Puff",
    emumaster: "Vc55$Te78+W9",
    NighthawkValga: "tgValga",
    Destroyer653100: "sri704529",
    TomDom83: "Tom12345",
    Kaylem: "Kaylem",
    blox: "blox",
    Jujutsu: "Trey",
    hunter: "Dixie696!",
    vampyrfang: "ssnph0lum",
  },
  routes: true, // Change this to false if you just want to host a bare server.
  local: true, // Change this to false to disable local assets.
}
export default config
